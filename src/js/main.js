import '../css/main.scss';
// import './responsive-menu.js';
import Caption from './components/caption.js';
import Popup from './components/popup.js';
import Tabs from './components/tabs.js';
import Edit from './utilities/edit-fields.js';


const editButtons = document.querySelectorAll('.edit-section__button'),
      navList = document.querySelector('.nav-tabs__list'),
      navTabs = document.querySelectorAll('.nav-tabs__tab'),
      tabs = document.querySelectorAll('.edit-section__tab'),
      followersBtn = document.querySelector('.profile-followers__add'),
      logout = document.querySelector('.profile-logout__button'),
      responsiveToggle = document.querySelector('.nav-tabs__toggle');


new Caption(followersBtn);
new Popup([logout, responsiveToggle]);
new Edit(editButtons);
new Tabs(navTabs, navList);
