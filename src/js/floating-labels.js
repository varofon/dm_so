export default class FloatingLabels {
  constructor(parent, input, label) {
    this.parent = parent;
    this.input = input;
    this.label = label;
    this.init();
  }

  init() {
    //If filled apply effect
    if (this.input.value !== '') {
      this.parent.classList.add('floating--active');
    }


    this.input.addEventListener('focus', () => {
      this.parent.classList.add('floating--focused');
    });

    this.input.addEventListener('blur', () => {
      this.parent.classList.remove('floating--focused');
    });


    this.input.addEventListener('change', () => {
      if (this.input.value === '') {
        this.parent.classList.remove('floating--active');
      } else {
        this.parent.classList.add('floating--active');
      }
    });
  }
}
