export default class EditButtons {
  constructor(parent, output) {
    this.parent = parent; //container for the buttons
    this.output = output; //output fields
  }

  createButtons() {
    let buttonContainer = document.createElement('div'),
        save = document.createElement('button'),
        cancel = document.createElement('button');

    buttonContainer.append(save, cancel);
    save.innerHTML = 'save';
    cancel.innerHTML = 'cancel';
    buttonContainer.className = 'edit-buttons';
    save.className = cancel.className = 'edit-buttons__button';


    cancel.addEventListener('click', () => {
      closeContainer(this.parent);
    });


    save.addEventListener('click', () => {

      for (let elem of this.output) {
        //select all output fields that match opened inputs
        let allOutputs = document.querySelectorAll(`[data-id=${elem.dataset.id}]`);
        //and fill them with inputs values
        for (let entry of allOutputs) {
          entry.innerHTML = this.parent.querySelector(`#${elem.dataset.id}`).value;
        }
      }

      closeContainer(this.parent);
    })

    return buttonContainer;
  }
}

function closeContainer(container) {
  container.remove();
}
