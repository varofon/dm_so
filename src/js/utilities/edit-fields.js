import EditButtons from './edit-buttons.js';
import EditInput from './edit-inputs.js';


export default class Edit {
  constructor(source) {
    this.source = source;
    this.init();
  }

  init() {
    for (let btn of this.source) {
      if (!btn.className.includes('button--mobile')) {
        btn.addEventListener('click', Edit.createPopup);
      } else {
        btn.addEventListener('click', Edit.editAll);
      }
    }
  }

  static createPopup() {
    Edit.clearPopup();

    let popup = document.createElement('div'), //popup container
        output = this.closest('.edit-section__output'), //ouput source
        outputFields = output.querySelectorAll('[data-id]'), //fields of output source
        outputCoord = output.getBoundingClientRect(),
        buttonCoord = this.getBoundingClientRect();

    popup.className = 'popup';
    popup.style.left = outputCoord.width - buttonCoord.width + 'px';

    for (let source of outputFields) {
      popup.append(new EditInput(source).createInput());
    }

    popup.append(new EditButtons(popup, outputFields).createButtons());

    this.after(popup);
  }

  //close other popups
  static clearPopup() {
    let popup = document.querySelector('.popup');
    if (popup) popup.remove();
  }


  static editAll() {
    let parent = this.closest('.edit-section__tab'),
        outputFields = parent.querySelectorAll('[data-id]'),
        content = parent.querySelector('.edit-section__content'),
        options = parent.querySelectorAll('.edit-section__option'),
        container = document.createElement('div');

    content.classList.add('edit-section__content--edited');

    for (let source of outputFields) {
      container.append(new EditInput(source).createInput());
    }

    container.append(new EditButtons(container, outputFields).createButtons());

    let buttons = container.querySelectorAll('.edit-buttons__button');

    for (let button of buttons) {
      button.addEventListener('click', function() {
        content.classList.remove('edit-section__content--edited');
      });
    }

    content.before(container);
  }
}
