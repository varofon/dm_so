import FloatingLabels from '../floating-labels.js';

export default class EditInput {
  constructor(source) {
    this.source = source;
  }

  createInput() {
    let label = document.createElement('label'),
      input = document.createElement('input'),
      span = document.createElement('span');
    label.prepend(input, span);
    label.className = 'floating';
    span.className = 'floating__label';
    input.className = 'floating__input';
    input.value = this.source.innerHTML;
    input.id = this.source.dataset.id;
    span.innerHTML = this.source.dataset.label;

    new FloatingLabels(label, input, span);
    return label;
  }
}
