export default class Caption {
  constructor(element) {
    this.element = element;
    this.init();
  }

  init() {
    this.element.addEventListener('mouseenter', this.showCaption);
    this.element.addEventListener('mouseleave', this.hideCaption);
  }


  showCaption() {
    if (document.querySelectorAll('.caption').length > 0) {
      return;
    }

    let caption = document.createElement('div');
    caption.classList.add('caption');
    caption.innerHTML = this.dataset.caption;
    caption.style.height = this.clientHegight + 'px';
    this.append(caption);
  }

  hideCaption() {
    if (document.querySelectorAll('.caption').length === 0) {
      return;
    }
    
    setTimeout(() => {
      document.querySelector('.caption').remove();
    }, 200);
  }

}
