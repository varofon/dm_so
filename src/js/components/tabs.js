export default class Tabs {
  constructor(navTabs, navList) {
    this.navTabs = navTabs; //collection of individual tabs
    this.navList = navList; //list that contains tabs
    this.init();
  }

  init() {
    for (let navTab of this.navTabs) {
      navTab.addEventListener('click', Tabs.switchTab);
    }
    this.mobileSwipe();
    this.responsiveToggle(this.navList);
  }


  static switchTab() {
    let tab = document.querySelector(`.edit-section__tab[data-tab=${this.dataset.tab}]`),
        navTabs = document.querySelectorAll('.nav-tabs__tab'),
        editTabs = document.querySelectorAll('.edit-section__tab');

    Tabs.resetTabs(editTabs, navTabs);
    this.classList.add('nav-tabs__tab--active');
    tab.classList.add('edit-section__tab--active');
  }


  static resetTabs(editTabs, navTabs) {
    for (let navTab of navTabs) {
      navTab.classList.remove('nav-tabs__tab--active');
    }
    for (let tab of editTabs) {
      tab.classList.remove('edit-section__tab--active');
    }
  }

  mobileSwipe() {
    let navCoord = null,
        touch = null,
        touchStart,
        distance = 0;

    this.navList.addEventListener('touchstart', e => {
      touch = e.changedTouches[0];
      navCoord = this.navList.getBoundingClientRect();
      touchStart = parseInt(touch.clientX) - navCoord.x;
    })

    this.navList.addEventListener('touchmove', e => {
      navCoord = this.navList.getBoundingClientRect();
      let leftLimit = navCoord.width - window.innerWidth;
      touch = e.changedTouches[0];
      distance = parseInt(touch.clientX) - touchStart;
      if (distance > 0) {
        distance = 0;
      } else if ((leftLimit + distance) < 0) {
        distance = '-' + parseInt(leftLimit);
      }
      this.navList.style.transform = 'translateX(' + distance + 'px)';
      e.preventDefault();
    })
  }

  responsiveToggle(navList) {
    let navListCollection = navList.querySelectorAll('li'),
        navListPop = document.querySelector('.nav-tabs__list--popup'),
        navListPopColl = document.querySelectorAll('.nav-tabs__list--popup li'),
        toggle = document.querySelector('.nav-tabs__toggle');

    navListPop.style.left = toggle.offsetLeft + 'px';

    window.addEventListener('resize', resizeFunc);
    resizeFunc();


    function resizeFunc() {
      let wrapped = false,
          firstLiOffset = navListCollection[0].offsetTop;
      navListPop.style.left = toggle.offsetLeft + 'px';
      for (let li of navListPopColl) {
        li.classList.remove('shown');
      }

      for (let li of navListCollection) {
        if (li.offsetTop > firstLiOffset) {
          wrapped = true;
          toggle.classList.add('shown');
          let popupLi = navListPop.querySelector(`[data-tab=${li.firstElementChild.dataset.tab}]`);
          popupLi.parentElement.classList.add('shown');
        }
      }
      if (!wrapped) {
        toggle.classList.remove('shown');
      }
    }
  }

}
