export default class Popup {
  constructor(elementsArr) {
    this.elementsArr = elementsArr;
    this.init();
  }


  init() {
    for (let elem of this.elementsArr) {
      elem.addEventListener('click', this.togglePopup);
    }

    document.addEventListener('click', this.closePopup);
  }



  togglePopup() {
    //open the popup
    this.classList.toggle('is-active');

    //get the total number of opened popups
    //if it's more then one, close the rest
    //but leave the current one
    if (document.querySelectorAll('.is-active').length > 1) {
      let openedPopups = document.querySelectorAll('.is-active');
      for (let popup of openedPopups) {
        popup.classList.remove('is-active');
      }
      //open back the current one
      this.classList.toggle('is-active');
    }
  }



  closePopup() {
    //if clicked inside of a popup do nothing
    if (event.target.parentElement.className.includes("popup-new") ||
      event.target.parentElement.className.includes("is-active") ||
      event.target.className.includes("popup-new") ||
      event.target.className.includes("is-active")
    ) {

      return;

    } else if (document.querySelectorAll('.is-active').length > 0) {

      document.querySelector('.is-active').classList.remove('is-active');

    }
  }
}
